var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Employee
var Employee = new Schema({
  firstName: {
	type : String
  },
  lastName: {
	type : String
  },
},{
    collection: 'employees'
});

module.exports = mongoose.model('Employee', Employee);